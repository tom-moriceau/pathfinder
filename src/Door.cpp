#include "pathfinder/Door.hpp"

using namespace pathfinder;

Door::Door(std::shared_ptr<IfcDoor> &ifc_door, std::shared_ptr<RepresentationConverter> converter)
{
    if (!ifc_door)
        throw std::runtime_error("Failed to create door : no IfcDoor provided");

    if (!ifc_door->m_Representation)
        throw std::runtime_error("Failed to create door : no representation provided for IfcDoor(" + utils::to_string(ifc_door->m_GlobalId->m_value) + ")");

    for (auto &representation : ifc_door->m_Representation->m_Representations)
    {
        if (representation->m_RepresentationIdentifier->m_value == L"Box")
        {
            if (representation->m_Items.size() == 0)
            {
                std::wcerr << "WARNING : IfcBoundingBox expected, no representation item found while processing IfcDoor(" << ifc_door->m_GlobalId->m_value << ")" << std::endl;
                continue;
            }

            std::shared_ptr<IfcBoundingBox> bounding_box = dynamic_pointer_cast<IfcBoundingBox>(representation->m_Items[0]);
            if (!bounding_box)
            {
                std::wcerr << "WARNING : IfcBoundingBox expected, " << representation->m_Items[0]->className() << " found while processing IfcDoor(" << ifc_door->m_GlobalId->m_value << ")" << std::endl;
                continue;
            }

            carve::geom3d::AABB aabb = utils::compute_aabb(bounding_box, converter, -3);
            m_bounding_box = utils::aabb_to_polyline(aabb);
            break;
        }

        if (representation->m_RepresentationType->m_value == L"Brep" || representation->m_RepresentationType->m_value == L"MappedRepresentation")
        {
            std::shared_ptr<RepresentationData> representation_data = std::make_shared<RepresentationData>();
            converter->convertIfcRepresentation(representation, representation_data);

            carve::geom3d::AABB aabb;
            representation_data->computeBoundingBox(aabb);
            m_bounding_box = utils::aabb_to_polyline(aabb);
            break;
        }
    }

    if(m_bounding_box.size() != 4)
        throw std::runtime_error("Failed to process IfcDoor(" + utils::to_string(ifc_door->m_GlobalId->m_value) + ")");

    double s1_len = (m_bounding_box[1] - m_bounding_box[0]).length();
    double s2_len = (m_bounding_box[2] - m_bounding_box[1]).length();

    double x_factor = 1;
    double y_factor = 1;
    if (s1_len >= s2_len)
        y_factor = RESCALE_FACTOR;
    if (s1_len <= s2_len)
        x_factor = RESCALE_FACTOR;

    carve::math::Matrix placement_matrix = utils::compute_placement(ifc_door->m_ObjectPlacement, converter);
    carve::math::Matrix scale_matrix = carve::math::Matrix::SCALE(x_factor, y_factor, 1);
    m_bounding_box = utils::transform(m_bounding_box, scale_matrix);
    m_bounding_box = utils::transform(m_bounding_box, placement_matrix);
}

Polygon2 Door::as_polygon() const
{
    Polygon2 p;
    p.push_back(m_bounding_box);
    p[0].push_back(m_bounding_box[0]);
    return p;
}

carve::geom2d::P2Vector Door::as_polyline() const
{
    return m_bounding_box;
}

json Door::to_json() const
{
    Polygon2 p;
    p.push_back(m_bounding_box);
    p[0].push_back(m_bounding_box[0]);
    return geojson::polygon_to_json(p);
}