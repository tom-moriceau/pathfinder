#include "pathfinder/geojson.hpp"

using namespace pathfinder;

json geojson::polygon_to_json(const Polygon2 &polygon)
{
    json ret;
    ret["type"] = "Polygon";
    ret["coordinates"] = {};
    for (auto &loop : polygon)
    {
        json json_loop = {};
        for (auto &pt : loop)
        {
            json_loop.push_back({pt.x, pt.y});
        }

        ret["coordinates"].push_back(json_loop);
    }

    return ret;
}

json geojson::polyline_to_json(const carve::geom2d::P2Vector &polyline)
{
    json ret;
    ret["type"] = "LineString";
    ret["coordinates"] = {};
    for (auto &pt : polyline)
    {
        ret["coordinates"].push_back({pt.x, pt.y});
    }

    return ret;
}

json geojson::style_to_json(const GeoJsonStyle style)
{
    json out;
    out["fill"] = style.fill_color;
    out["fill-opacity"] = style.opacity;

    return out;
}