#include "pathfinder/geometry.hpp"

using namespace pathfinder;

//
// Utility functions
//
Clipper2Lib::PathsD polygon_to_pathsd(const Polygon2 &polygon)
{
    Clipper2Lib::PathsD paths;
    for (auto &loop : polygon)
    {
        Clipper2Lib::PathD loopd;
        for (auto &pt : loop)
        {
            loopd.push_back({pt.x, pt.y});
        }
        paths.push_back(loopd);
    }

    return paths;
}

Clipper2Lib::PathsD polygons_to_pathsd(const std::vector<Polygon2> &polygons)
{
    Clipper2Lib::PathsD paths;
    for (auto &poly : polygons)
    {
        Clipper2Lib::PathsD tmp_paths = polygon_to_pathsd(poly);
        paths.insert(paths.end(), tmp_paths.begin(), tmp_paths.end());
    }

    return paths;
}

carve::geom2d::P2Vector pathd_to_polyline(const Clipper2Lib::PathD &pathd)
{
    carve::geom2d::P2Vector loop;
    for (auto &pt : pathd)
    {
        loop.push_back(carve::geom::VECTOR(pt.x, pt.y));
    }
    return loop;
}

Clipper2Lib::PathD polyline_to_pathd(const carve::geom2d::P2Vector &loop)
{
    Clipper2Lib::PathD pathd;
    for (auto &pt : loop)
    {
        pathd.push_back({pt.x, pt.y});
    }
    return pathd;
}

//
// Public functions
//

std::vector<carve::geom2d::P2Vector> utils::slit_to_segments(const carve::geom2d::P2Vector &polyline)
{
    std::vector<carve::geom2d::P2Vector> ret;
    for (size_t i = 0; i < polyline.size() - 1; i++)
    {
        ret.push_back({polyline[i], polyline[i + 1]});
    }
    return ret;
}

carve::geom2d::P2Vector utils::join_from_segments(const std::vector<carve::geom2d::P2Vector> &segments)
{
    carve::geom2d::P2Vector ret;
    if (segments.size() == 0) return ret;

    ret.push_back(segments[0][0]);
    for (auto &segment : segments)
    {
        if (segment.size() != 2)
            continue;
        
        ret.push_back(segment[1]);
    }

    return ret;
}

std::vector<Polygon2> utils::polygon_boolean(const Polygon2 &subject, const Polygon2 &clip, PolygonBooleanOperation op)
{
    Clipper2Lib::PathsD clipper_subject = polygon_to_pathsd(subject);
    Clipper2Lib::PathsD clipper_clip = polygon_to_pathsd(clip);

    Clipper2Lib::PathsD solutions;
    switch (op)
    {
    case PolygonUnion:
        clipper_subject.insert(clipper_subject.end(), clipper_clip.begin(), clipper_clip.end());
        solutions = Clipper2Lib::Union(clipper_subject, Clipper2Lib::FillRule::NonZero, 3);
        break;
    case PolygonIntersection:
        solutions = Clipper2Lib::Intersect(clipper_subject, clipper_clip, Clipper2Lib::FillRule::EvenOdd, 3);
        break;
    case PolygonDifference:
        solutions = Clipper2Lib::Difference(clipper_subject, clipper_clip, Clipper2Lib::FillRule::EvenOdd, 3);
        break;
    case PolygonXor:
        solutions = Clipper2Lib::Xor(clipper_subject, clipper_clip, Clipper2Lib::FillRule::EvenOdd, 3);
        break;
    }

    std::vector<Polygon2> ret;
    Polygon2 current_poly;
    for (auto sol_iter = solutions.begin(); sol_iter != solutions.end(); )
    {
        if (sol_iter == solutions.begin() && !Clipper2Lib::IsPositive(*sol_iter))
            break;

        current_poly.push_back(pathd_to_polyline(*sol_iter));
        sol_iter++;
        while (sol_iter != solutions.end() && !Clipper2Lib::IsPositive(*sol_iter))
        {
            current_poly.push_back(pathd_to_polyline(*sol_iter));
            sol_iter++;
        }

        ret.push_back(current_poly);
        current_poly.clear();
    }
    
    return ret;
}

std::vector<Polygon2> utils::polygon_union(const std::vector<Polygon2> &subjects)
{
    Clipper2Lib::PathsD paths = polygons_to_pathsd(subjects);
    Clipper2Lib::PathsD solutions = Clipper2Lib::Union(paths, Clipper2Lib::FillRule::NonZero, 3);

    std::vector<Polygon2> ret;
    Polygon2 current_poly;
    for (auto sol_iter = solutions.begin(); sol_iter != solutions.end();)
    {
        if (sol_iter == solutions.begin() && !Clipper2Lib::IsPositive(*sol_iter))
            break;

        current_poly.push_back(pathd_to_polyline(*sol_iter));
        sol_iter++;
        while (sol_iter != solutions.end() && !Clipper2Lib::IsPositive(*sol_iter))
        {
            current_poly.push_back(pathd_to_polyline(*sol_iter));
            sol_iter++;
        }

        ret.push_back(current_poly);
        current_poly.clear();
    }

    return ret;
}

std::vector<carve::geom2d::P2Vector> utils::polyline_boolean(const carve::geom2d::P2Vector &subject, const Polygon2 &clip, PolylineBooleanOperation op)
{
    Clipper2Lib::PathsD subject_paths;
    subject_paths.push_back(polyline_to_pathd(subject));
    Clipper2Lib::PathsD clip_paths = polygon_to_pathsd(clip);

    Clipper2Lib::ClipperD clipper(3);
    clipper.AddOpenSubject(subject_paths);
    clipper.AddClip(clip_paths);

    Clipper2Lib::ClipType clip_type;
    switch (op)
    {
    case PolylineIntersection:
        clip_type = Clipper2Lib::ClipType::Intersection;
        break;
    case PolylineDifference:
        clip_type = Clipper2Lib::ClipType::Difference;
        break;
    }

    Clipper2Lib::PathsD closed_solutions;
    Clipper2Lib::PathsD open_solutions;
    clipper.Execute(clip_type, Clipper2Lib::FillRule::EvenOdd, closed_solutions, open_solutions);

    std::vector<carve::geom2d::P2Vector> ret;
    for (auto &open_solution : open_solutions)
    {
        ret.push_back(pathd_to_polyline(open_solution));
    }

    return ret;
}

std::vector<carve::geom2d::P2Vector> utils::polyline_boolean(const std::vector<carve::geom2d::P2Vector> &subjects, const std::vector<Polygon2> &clips, PolylineBooleanOperation op)
{
    Clipper2Lib::PathsD subject_paths;
    for (auto &subject : subjects)
        subject_paths.push_back(polyline_to_pathd(subject));

    Clipper2Lib::PathsD clip_paths;
    for (auto &clip : clips)
    {
        Clipper2Lib::PathsD paths = polygon_to_pathsd(clip);
        clip_paths.insert(clip_paths.end(), paths.begin(), paths.end());
    }

    Clipper2Lib::ClipperD clipper(3);
    clipper.AddOpenSubject(subject_paths);
    clipper.AddClip(clip_paths);

    Clipper2Lib::ClipType clip_type;
    switch (op)
    {
    case PolylineIntersection:
        clip_type = Clipper2Lib::ClipType::Intersection;
        break;
    case PolylineDifference:
        clip_type = Clipper2Lib::ClipType::Difference;
        break;
    }

    Clipper2Lib::PathsD closed_solutions;
    Clipper2Lib::PathsD open_solutions;
    clipper.Execute(clip_type, Clipper2Lib::FillRule::EvenOdd, closed_solutions, open_solutions);

    std::vector<carve::geom2d::P2Vector> ret;
    for (auto &open_solution : open_solutions)
    {
        ret.push_back(pathd_to_polyline(open_solution));
    }

    return ret;
}