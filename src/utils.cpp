#include "pathfinder/utils.hpp"

using namespace pathfinder;

using convert_type = std::codecvt_utf8<wchar_t>;
std::wstring_convert<convert_type, wchar_t> converter;

std::string utils::to_string(const std::wstring &wstring)
{
    return converter.to_bytes(wstring);
}

std::wstring utils::to_wstring(const std::string &string)
{
    return converter.from_bytes(string);
}

carve::geom3d::AABB utils::compute_aabb(std::shared_ptr<IfcBoundingBox> bbox, std::shared_ptr<RepresentationConverter> representation_converter, const int32_t precision)
{
    vec3 min_bound;
    vec3 max_bound;

    min_bound.x = bbox->m_Corner->m_Coordinates[0]->m_value;
    min_bound.y = bbox->m_Corner->m_Coordinates[1]->m_value;
    min_bound.z = bbox->m_Corner->m_Coordinates[2]->m_value;

    max_bound.x = min_bound.x + bbox->m_XDim->m_value;
    max_bound.y = min_bound.y + bbox->m_YDim->m_value;
    max_bound.z = min_bound.z + bbox->m_ZDim->m_value;

    min_bound *= std::pow(10, precision);
    max_bound *= std::pow(10, precision);

    vec3 pos = (max_bound + min_bound) / 2.;
    vec3 extends = (max_bound - min_bound) / 2.;

    return carve::geom3d::AABB(pos, extends);
}

Polygon2 utils::compute_profile(std::shared_ptr<IfcSweptAreaSolid> solid_model, std::shared_ptr<RepresentationConverter> representation_converter)
{
    std::shared_ptr<ProfileConverter> profile_converter = representation_converter->getProfileCache()->getProfileConverter(solid_model->m_SweptArea);
    Polygon2 polygon = profile_converter->getCoordinates();

    for (auto &loop : polygon)
    {
        if (loop.size() > 1)
        {
            loop.push_back(loop[0]);
        }
    }

    return polygon;
}

carve::math::Matrix utils::compute_placement(std::shared_ptr<IfcObjectPlacement> placement, std::shared_ptr<RepresentationConverter> representation_converter)
{
    std::shared_ptr<ProductShapeData> product_data = std::make_shared<ProductShapeData>();
    std::unordered_set<IfcObjectPlacement*> placement_already_applied;
    representation_converter->getPlacementConverter()->convertIfcObjectPlacement(placement, product_data, placement_already_applied, false);

    return product_data->getTransform();
}

carve::math::Matrix utils::compute_placement(std::shared_ptr<IfcPlacement> placement, std::shared_ptr<RepresentationConverter> representation_converter)
{
    std::shared_ptr<TransformData> transform_data = std::make_shared<TransformData>();
    representation_converter->getPlacementConverter()->convertIfcPlacement(placement, transform_data, false);

    return transform_data->m_matrix;
}

Polygon2 utils::polylineset_to_polygon(const std::shared_ptr<carve::input::PolylineSetData> polyline_set)
{
    Polygon2 ret;
    for (auto &loop : polyline_set->polylines)
    {
        carve::geom2d::P2Vector path;
        for (auto &idx : loop.second)
        {
            vec3 pt3 = polyline_set->points[idx];
            vec2 pt = carve::geom::VECTOR(pt3.x, pt3.y);
            path.push_back(pt);
        }

        ret.push_back(path);
    }

    return ret;
}


carve::geom2d::P2Vector utils::aabb_to_polyline(const carve::geom3d::AABB& aabb)
{
    carve::geom2d::P2Vector polyline;
    polyline.push_back(carve::geom::VECTOR(aabb.min().x, aabb.min().y));
    polyline.push_back(carve::geom::VECTOR(aabb.max().x, aabb.min().y));
    polyline.push_back(carve::geom::VECTOR(aabb.max().x, aabb.max().y));
    polyline.push_back(carve::geom::VECTOR(aabb.min().x, aabb.max().y));

    return polyline;
}

void utils::close_polygon(Polygon2& poly)
{
    for(auto &loop : poly)
    {
        if(loop[0] != loop[loop.size() - 1])
            loop.push_back(loop[0]);
    }
}

carve::geom2d::P2Vector utils::transform(const carve::geom2d::P2Vector& loop, const carve::math::Matrix &transform)
{
    carve::geom2d::P2Vector ret_loop;
    for (auto &pt : loop)
    {
        vec3 ret_pt3 = transform * carve::geom::VECTOR(pt.x, pt.y, 0);
        carve::geom2d::P2 ret_pt;
        ret_pt.x = ret_pt3.x;
        ret_pt.y = ret_pt3.y;
        ret_loop.push_back(ret_pt);
    }

    return ret_loop;
}

Polygon2 utils::transform(const Polygon2& poly, const carve::math::Matrix &transform)
{
    Polygon2 ret;
    for (auto &loop : poly)
    {
        ret.push_back(utils::transform(loop, transform));
    }
    
    return ret;
}

std::ostream &operator<<(std::ostream &os, const carve::geom2d::P2Vector &polyline)
{
    os << "[";
    std::string v_sep = "";
    for (auto &v : polyline)
    {
        os << v_sep << "[" << v.x << "," << v.y << "]";
        v_sep = ",";
    }
    os << "]";

    return os;
}

std::ostream &operator<<(std::ostream &os, const pathfinder::Polygon2 &polygon)
{
    os << "[";
    std::string p_sep = "";
    for (auto &polyline : polygon)
    {
        os << p_sep << polyline;
        p_sep = ",";
    }
    os << "]";

    return os;
}