#include <iostream>
#include <ctime>

#include "ifcpp/reader/ReaderSTEP.h"
#include "ifcpp/IFC4/include/IfcBuildingStorey.h"

#include "pathfinder/Storey.hpp"
#include "pathfinder/Building.hpp"
#include "pathfinder/utils.hpp"
#include "pathfinder/geometry.hpp"

carve::geom2d::P2Vector pathd_to_polyline_pers(const Clipper2Lib::PathD &pathd)
{
    carve::geom2d::P2Vector loop;
    for (auto &pt : pathd)
    {
        loop.push_back(carve::geom::VECTOR(pt.x, pt.y));
    }
    return loop;
}

int main(int argc, char const *argv[])
{
    // if (argc == 1) {
    //     std::cout << "Usage :\n  " << argv[0] << " <input_filename> [<output_filename>]" << std::endl;
    // }

    ReaderSTEP reader_step;
    std::shared_ptr<BuildingModel> building_model = std::make_shared<BuildingModel>();
    // reader_step.loadModelFromFile(L"../assets/DigitalHub.ifc", building_model);
    reader_step.loadModelFromFile(L"../assets/IFC Schependomlaan.ifc", building_model);

    clock_t t0 = clock();
    pathfinder::Building building(building_model);
    building.compute_path_networks();

    std::cout << building.to_json().dump();
    clock_t t1 = clock();

    std::cerr << "NOTE : IFC parsing done in " << (double)(t1-t0)/CLOCKS_PER_SEC << "s" << std::endl;

    return 0;
}
