#include "pathfinder/Room.hpp"

using namespace pathfinder;

Room::Room(std::shared_ptr<IfcSpace> &ifc_space, std::shared_ptr<RepresentationConverter> converter, size_t storey_idx, size_t room_idx)
{
    if (!ifc_space)
        throw std::runtime_error("Failed to create room : no IfcSpace provided");
    if (!ifc_space->m_Representation)
        throw std::runtime_error("Failed to create room : no representation provided for IfcSpace(" + utils::to_string(ifc_space->m_GlobalId->m_value) + ")");

    m_storey_id = storey_idx;
    m_id = room_idx;

    carve::math::Matrix room_placement = utils::compute_placement(ifc_space->m_ObjectPlacement, converter);

    // Find and process representation
    bool representation_processed = false;
    for (auto &representation : ifc_space->m_Representation->m_Representations)
    {
        if (representation->m_RepresentationIdentifier->m_value == L"FootPrint")
        {
            std::shared_ptr<RepresentationData> representation_data = std::make_shared<RepresentationData>();
            converter->convertIfcRepresentation(representation, representation_data);

            if (representation_data->m_vec_item_data.size() > 0) {
                for (auto &carve_polyline : representation_data->m_vec_item_data[0]->m_polylines) {
                    Polygon2 room_poly = utils::polylineset_to_polygon(carve_polyline);
                    m_polygons.push_back(room_poly);
                }
            }

            representation_processed = true;
            break;
        }

        // If representation isn't SweptSolid, we don't use it
        if (representation->m_RepresentationType->m_value == L"SweptSolid")
        {
            for (auto &representation_item : representation->m_Items)
            {
                std::shared_ptr<IfcSweptAreaSolid> solid = dynamic_pointer_cast<IfcSweptAreaSolid>(representation_item);
                if (!solid)
                {
                    std::wcerr << "WARNING : IfcSweptAreaSolid expected, " << representation_item->className() << " found while processing IfcSpace(" << ifc_space->m_GlobalId->m_value << ")" << std::endl;
                    continue;
                }

                Polygon2 profile_poly = utils::compute_profile(solid, converter);
                carve::math::Matrix solid_matrix = utils::compute_placement(solid->m_Position, converter);
                profile_poly = utils::transform(profile_poly, solid_matrix);
                m_polygons.push_back(profile_poly);
            }
            representation_processed = true;
            break;
        }
    }

    // If no representation has been found, raise an error
    if (!representation_processed) {
        std::string representation_list = "[";
        std::string sep = "";
        for (auto &representation : ifc_space->m_Representation->m_Representations)
        {
            representation_list += sep + utils::to_string(representation->m_RepresentationIdentifier->m_value) + "/" + utils::to_string(representation->m_RepresentationType->m_value);
            sep = ",";
        }
        representation_list += "]";
        throw std::runtime_error("Failed to create room : no supported representation provided for IfcSpace(" + utils::to_string(ifc_space->m_GlobalId->m_value) + ") " + representation_list);
    }

    for (auto &poly : m_polygons)
    {
        poly = utils::transform(poly, room_placement);
    }
}

Room::Room(std::vector<Polygon2> polygons, size_t storey_idx, size_t room_idx)
{
    m_polygons = polygons;
    m_storey_id = storey_idx;
    m_id = room_idx;
}

double Room::door_covering_area(const DoorPtr door) const
{
    double area = 0;
    Polygon2 door_poly = door->as_polygon();
    for (auto &room_poly : m_polygons)
    {
        std::vector<Polygon2> intersects = utils::polygon_boolean(door_poly, room_poly, utils::PolygonIntersection);
        for(auto &intersect : intersects)
        {
            for (auto &loop : intersect)
                area += carve::geom2d::signedArea(loop);
        }
    }
    
    return area;
}

void Room::add_door(DoorPtr door)
{
    if(!door)
        return;
    m_doors.insert(door);
}

void Room::remove_door(DoorPtr door)
{
    if (!door)
        return;
    m_doors.erase(door);
}

void Room::add_connected_door(DoorPtr door)
{
    if (!door)
        return;
    m_connected_doors.insert(door);
}

void Room::remove_connected_door(DoorPtr door)
{
    if (!door)
        return;
    m_connected_doors.erase(door);
}

std::string Room::get_id_str() const
{
    return "L" + std::to_string(m_storey_id) + "_R" + std::to_string(m_id);
}

size_t Room::get_storey_id() const
{
    return m_storey_id;
}

size_t Room::get_room_id() const
{
    return m_id;
}

std::vector<Polygon2> Room::get_polygons() const
{
    return m_polygons;
}

std::vector<Polygon2> Room::get_complete_polygons() const
{
    std::vector<Polygon2> doors_polys;
    for (auto &door : m_doors)
    {
        doors_polys.push_back(door->as_polygon());
    }

    std::vector<Polygon2> polygons_total = m_polygons;
    polygons_total.insert(polygons_total.end(), doors_polys.begin(), doors_polys.end());

    return utils::polygon_union(polygons_total);
}

std::vector<carve::geom2d::P2Vector> Room::get_complete_walls() const
{
    std::vector<Polygon2> doors_polys;
    for (auto &door : m_doors)
        doors_polys.push_back(door->as_polygon());
    for (auto &door : m_connected_doors)
        doors_polys.push_back(door->as_polygon());

    std::vector<carve::geom2d::P2Vector> room_outer_walls;
    for (auto &room_poly : m_polygons)
    {
        if (room_poly.size() > 0)
        {
            std::vector<carve::geom2d::P2Vector> segment_wall = utils::slit_to_segments(room_poly[0]);
            std::vector<carve::geom2d::P2Vector> cutted_wall = utils::polyline_boolean(segment_wall, doors_polys, utils::PolylineDifference);
            room_outer_walls.insert(room_outer_walls.end(), cutted_wall.begin(), cutted_wall.end());
        }
    }

    return room_outer_walls;
}

json Room::to_json() const
{
    json json_room = {};
    std::vector<Polygon2> complete_polygons = get_complete_polygons();
    for (auto &path : complete_polygons)
    {
        json json_polygon;
        json_polygon["type"] = "Feature";
        json_polygon["id"] = get_id_str();
        json_polygon["geometry"] = geojson::polygon_to_json(path);

        json_room.push_back(json_polygon);
    }

    std::vector<carve::geom2d::P2Vector> walls = get_complete_walls();
    size_t wall_idx = 0;
    for (auto &wall : walls)
    {
        json json_linestring;
        json_linestring["type"] = "Feature";
        json_linestring["id"] = get_id_str() + "_T" + std::to_string(wall_idx);
        json_linestring["geometry"] = geojson::polyline_to_json(wall);

        json_room.push_back(json_linestring);
        wall_idx++;
    }

    return json_room;
}