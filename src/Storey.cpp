#include "pathfinder/Storey.hpp"

using namespace pathfinder;

Storey::Storey(std::shared_ptr<IfcBuildingStorey> &ifc_storey, std::shared_ptr<RepresentationConverter> converter, size_t storey_idx)
{
    if (!ifc_storey)
        throw std::runtime_error("Failed to create storey : no IfcBuildingStorey provided");

    m_id = storey_idx;

    for (auto &weak_decomposed : ifc_storey->m_IsDecomposedBy_inverse)
    {
        std::shared_ptr<IfcRelAggregates> shared_decomposed(weak_decomposed);
        for (auto &related_object : shared_decomposed->m_RelatedObjects)
        {
            std::shared_ptr<IfcSpace> ifc_space = dynamic_pointer_cast<IfcSpace>(related_object);
            if (ifc_space)
            {
                try
                {
                    RoomPtr room = std::make_shared<Room>(ifc_space, converter, storey_idx, m_rooms.size());
                    m_rooms.insert(room);
                }
                catch (const std::runtime_error &e)
                {
                    std::cerr << "WARNING : " << e.what() << std::endl;
                }
            }
        }
    }

    for (auto &weak_contained : ifc_storey->m_ContainsElements_inverse)
    {
        std::shared_ptr<IfcRelContainedInSpatialStructure> shared_contained(weak_contained);

        for (auto &contained_object : shared_contained->m_RelatedElements)
        {
            std::shared_ptr<IfcDoor> ifc_door = dynamic_pointer_cast<IfcDoor>(contained_object);
            if (ifc_door)
            {
                try
                {
                    DoorPtr door = std::make_shared<Door>(ifc_door, converter);
                    m_doors.insert(door);
                }
                catch (const std::runtime_error &e)
                {
                    std::cerr << "WARNING : " << e.what() << std::endl;
                }
            }
        }
    }
}

void Storey::merge_intersecting_rooms()
{
    bool changed;
    do {
        changed = false;
        std::set<RoomPtr> rooms_to_delete;
        std::set<RoomPtr> rooms_to_add;
        std::set<std::pair<RoomPtr, RoomPtr>> rooms_processed;
        for (auto &room1 : m_rooms)
        {
            std::vector<Polygon2> room1_polygons = room1->get_polygons();
            for (auto &room2 : m_rooms)
            {
                if (room1 == room2)
                    continue;

                std::pair<RoomPtr, RoomPtr> current_pair;
                if (room1 < room2)
                    current_pair = std::make_pair(room1, room2);
                else
                    current_pair = std::make_pair(room2, room1);
                
                if(rooms_processed.count(current_pair) > 0)
                    continue;
                else
                    rooms_processed.insert(current_pair);

                std::vector<Polygon2> room2_polygons = room2->get_polygons();
                std::vector<Polygon2> total_room_polygons = room1_polygons;
                total_room_polygons.insert(total_room_polygons.end(), room2_polygons.begin(), room2_polygons.end());
                std::vector<Polygon2> unioned = utils::polygon_union(total_room_polygons);

                if (unioned.size() != total_room_polygons.size())
                {
                    changed = true;
                    rooms_to_add.insert(std::make_shared<Room>(unioned, room1->get_storey_id(), room1->get_room_id()));
                    rooms_to_delete.insert(room1);
                    rooms_to_delete.insert(room2);
                }
            }
        }

        for (auto &room_to_delete : rooms_to_delete)
            m_rooms.erase(room_to_delete);
        for (auto &room_to_add : rooms_to_add)
            m_rooms.insert(room_to_add);

        rooms_to_add.clear();
        rooms_to_delete.clear();

    } while (changed);
}

void Storey::assign_door_to_room()
{
    for (auto &door : m_doors)
    {
        Polygon2 door_poly = door->as_polygon();
        RoomPtr main_room = nullptr;
        std::set<RoomPtr> connected_rooms;
        double max_intersect_area = 0;
        for (auto &room : m_rooms)
        {
            double door_intersect_area = std::abs(room->door_covering_area(door));
            if (door_intersect_area > max_intersect_area)
            {
                if (main_room)
                    connected_rooms.insert(main_room);
                max_intersect_area = door_intersect_area;
                main_room = room;
            } else {
                connected_rooms.insert(room);
            }
        }

        if(main_room)
            main_room->add_door(door);

        for (auto &connected_room : connected_rooms)
            connected_room->add_connected_door(door);
        
    }
}

std::vector<Polygon2> Storey::get_single_polygons() const {
    std::vector<Polygon2> ret;

    for (auto &room : m_rooms)
    {
        std::vector<Polygon2> room_polygons = room->get_complete_polygons();
        ret.insert(ret.end(), room_polygons.begin(), room_polygons.end());
    }
    
    ret = utils::polygon_union(ret);
    return ret;
}

json Storey::to_json() const
{
    json json_storey = json::array({});
    std::vector<Polygon2> room_polygons = get_single_polygons();
    size_t room_idx = 0;
    for (auto &room_polygon : room_polygons)
    {
        std::string room_id = "L" + std::to_string(m_id) + "_R" + std::to_string(room_idx);

        json json_room;
        json_room["type"] = "Feature";
        json_room["id"] = room_id;
        json_room["geometry"] = geojson::polygon_to_json(room_polygon);

        size_t wall_idx = 0;
        for (auto &loop : room_polygon)
        {
            json json_wall;
            json_wall["type"] = "Feature";
            json_wall["id"] = room_id + "_W" + std::to_string(wall_idx);
            json_wall["geometry"] = geojson::polyline_to_json(loop);

            json_storey.push_back(json_wall);
            wall_idx++;
        }
        json_storey.push_back(json_room);

        room_idx++;
    }
    

    return json_storey;
}