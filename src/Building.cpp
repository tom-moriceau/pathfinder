#include "pathfinder/Building.hpp"

using namespace pathfinder;

Building::Building(std::shared_ptr<BuildingModel> ifc_model) : m_geometry_converter(GeometryConverter(ifc_model))
{
    m_geometry_converter.setModel(ifc_model);
    m_geometry_converter.convertGeometry();

    std::vector<std::shared_ptr<IfcBuildingStorey>> ifc_storeys;

    for (auto &entity : ifc_model->getMapIfcEntities())
    {
        std::shared_ptr<IfcBuildingStorey> ifc_storey = dynamic_pointer_cast<IfcBuildingStorey>(entity.second);
        if (ifc_storey)
            ifc_storeys.push_back(ifc_storey);
    }
    std::sort(
        ifc_storeys.begin(),
        ifc_storeys.end(),
        [](const std::shared_ptr<IfcBuildingStorey> &a, const std::shared_ptr<IfcBuildingStorey> &b) -> bool
        {
            return a->m_Elevation->m_value < b->m_Elevation->m_value;
        }
    );

    for (auto &ifc_storey : ifc_storeys)
    {
        StoreyPtr storey = std::make_shared<Storey>(ifc_storey, m_geometry_converter.getRepresentationConverter(), m_storeys.size());
        storey->merge_intersecting_rooms();
        storey->assign_door_to_room();
        m_storeys.push_back(storey);
    }
}

void Building::compute_path_networks()
{
    PathNetwork network;
    for (auto &storey : m_storeys)
    {
        for (auto &polygon : storey->get_single_polygons())
        {
            network = PathNetwork(polygon);
            network.compute_network();
            m_path_networks.push_back(network);
        }
    }
}

json Building::to_json() const
{
    json collection;
    collection["type"] = "FeatureCollection";
    collection["features"] = json::array({});
    collection["storeys"] = json::array({});
    int i = 0;
    for(auto &storey : m_storeys)
    {
        json json_storey = storey->to_json();
        collection["features"].insert(collection["features"].end(), json_storey.begin(), json_storey.end());

        collection["storeys"].push_back("L" + std::to_string(i));
        i++;
    }

    for (auto &network : m_path_networks)
    {
        json json_network = network.to_json();
        collection["features"].insert(collection["features"].end(), json_network.begin(), json_network.end());
    }

    return collection;
}