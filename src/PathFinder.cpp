#include "pathfinder/PathFinder.hpp"

using namespace pathfinder;

Node::Node() : x(0), y(0)
{
}

Node::Node(double x, double y) : x(x), y(y)
{
}

Node::Node(carve::geom2d::P2 pt) : x(pt.x), y(pt.y)
{
}

void Node::add_connection(Connection connection)
{
    m_connected_nodes.insert(connection);
}

void Node::add_connection(double distance, std::shared_ptr<Node> target)
{
    m_connected_nodes.insert({ distance, target });
}

void Node::remove_connection(Connection connection)
{
    m_connected_nodes.erase(connection);
}

void Node::remove_connection(std::shared_ptr<Node> target)
{
    m_connected_nodes.erase({ 0, target });
}

const std::set<Connection, Node::ConnectionComp> &Node::get_connections() const
{
    return m_connected_nodes;
}

double PathNetwork::connect(NodePtr node1, NodePtr node2) const
{
    return 1;
}

PathNetwork::PathNetwork(const Polygon2 &polygon)
{
    m_polygon = polygon;

    for (auto &wall : polygon)
    {
        std::vector<carve::geom2d::P2Vector> segments = utils::slit_to_segments(wall);
        m_walls_segments.insert(m_walls_segments.end(), segments.begin(), segments.end());

        for (auto &pt : wall)
            m_nodes.insert(std::make_shared<Node>(pt));
    }
}

void PathNetwork::compute_network()
{
    std::set<std::pair<NodePtr, NodePtr>> processed_connection;
    for (auto &node1 : m_nodes)
    {
        for (auto &node2 : m_nodes)
        {
            if(node1 == node2)
                continue;
            std::pair<NodePtr, NodePtr> current_pair = std::make_pair(std::min(node1, node2), std::max(node1, node2));
            if(processed_connection.count(current_pair) != 0)
                continue;

            double distance = connect(node1, node2);
            if(distance >= 0)
            {
                node1->add_connection({distance, node2});
                node2->add_connection({distance, node1});
            }

            processed_connection.insert(current_pair);
        }
    }
}

json PathNetwork::to_json() const
{
    json json_connections = json::array({});
    std::set<std::pair<NodePtr, NodePtr>> processed_connection;
    for (auto &node : m_nodes)
    {
        for (auto &connection : node->get_connections())
        {
            std::pair<NodePtr, NodePtr> current_pair = std::make_pair(std::min(node, connection.target), std::max(node, connection.target));
            if (processed_connection.count(current_pair) != 0)
                continue;

            json json_conn;
            json_conn["type"] = "Feature";
            json_conn["geometry"]["type"] = "LineString";
            json_conn["geometry"]["coordinates"] = json::array({});
            json_conn["geometry"]["coordinates"].push_back({node->x, node->y});
            json_conn["geometry"]["coordinates"].push_back({connection.target->x, connection.target->y});

            json_connections.push_back(json_conn);
        }
    }
    
    return json_connections;
}

bool operator==(const Connection &a, const Connection &b)
{
    return a.target == b.target;
}

bool operator!=(const Connection &a, const Connection &b)
{
    return !(a == b);
}

bool operator<(const pathfinder::Connection &a, const pathfinder::Connection &b)
{
    if (a.target < b.target)
        return true;
    return a.distance < b.distance;
}

bool operator==(const Node &a, const Node &b)
{
    return a.x == b.x && a.y == b.y;
}

bool operator!=(const Node &a, const Node &b)
{
    return !(a == b);
}