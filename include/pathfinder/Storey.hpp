#pragma once

#include <set>
#include <utility>

#include "ifcpp/IFC4/include/IfcBuildingStorey.h"
#include "ifcpp/IFC4/include/IfcRelAggregates.h"
#include "ifcpp/IFC4/include/IfcRelContainedInSpatialStructure.h"
#include "ifcpp/geometry/Carve/RepresentationConverter.h"

#include "nlohmann/json.hpp"

#include "pathfinder/Room.hpp"
#include "pathfinder/geometry.hpp"

using json = nlohmann::json;

namespace pathfinder
{
    class Storey
    {
    private:
        size_t m_id;
        std::set<RoomPtr> m_rooms;
        std::set<DoorPtr> m_doors;

    public:
        Storey(std::shared_ptr<IfcBuildingStorey> &ifc_storey, std::shared_ptr<RepresentationConverter> converter, size_t storey_idx);

        void merge_intersecting_rooms();
        void assign_door_to_room();
        std::vector<Polygon2> get_single_polygons() const;

        json to_json() const;
    };

    typedef std::shared_ptr<Storey> StoreyPtr;
}