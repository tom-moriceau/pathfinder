#pragma once

#include <set>
#include <memory>

#include "carve/geom2d.hpp"
#include "nlohmann/json.hpp"

#include "pathfinder/Storey.hpp"
#include "pathfinder/geometry.hpp"

using json = nlohmann::json;

namespace pathfinder {

    class Node;

    struct Connection
    {
        double distance;
        std::shared_ptr<Node> target;
    };

    class Node
    {
    private:
        typedef struct {
            bool operator()(const Connection &a, const Connection &b)
            {
                if (a.target < b.target)
                    return true;
                return a.distance < b.distance;
            }
        } ConnectionComp;

        std::set<Connection, ConnectionComp> m_connected_nodes;

    public:
        double x, y;

        Node();
        Node(double x, double y);
        Node(carve::geom2d::P2 pt);

        void add_connection(Connection connection);
        void add_connection(double distance, std::shared_ptr<Node> target);
        void remove_connection(Connection connection);
        void remove_connection(std::shared_ptr<Node> target);
        const std::set<Connection, ConnectionComp> &get_connections() const;
    };

    typedef std::shared_ptr<Node> NodePtr;

    class PathNetwork
    {
    private:
        Polygon2 m_polygon;
        std::vector<carve::geom2d::P2Vector> m_walls_segments;
        std::set<NodePtr> m_nodes;

        double connect(NodePtr node1, NodePtr node2) const;

    public:
        PathNetwork() {}
        PathNetwork(const Polygon2 &polygon);

        void compute_network();
        json to_json() const;
    };
};

bool operator==(const pathfinder::Connection &a, const pathfinder::Connection &b);
bool operator!=(const pathfinder::Connection &a, const pathfinder::Connection &b);
bool operator==(const pathfinder::Node &a, const pathfinder::Node &b);
bool operator!=(const pathfinder::Node &a, const pathfinder::Node &b);