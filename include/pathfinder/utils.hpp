#pragma once

#include <string>
#include <locale>
#include <codecvt>

#include "carve/geom2d.hpp"
#include "carve/matrix.hpp"
#include "ifcpp/IFC4/include/IfcBoundingBox.h"
#include "ifcpp/IFC4/include/IfcSweptAreaSolid.h"
#include "ifcpp/IFC4/include/IfcObjectPlacement.h"
#include "ifcpp/IFC4/include/IfcPlacement.h"
#include "ifcpp/geometry/Carve/RepresentationConverter.h"

namespace pathfinder
{
    typedef std::vector<std::vector<carve::geom2d::P2>> Polygon2;

    namespace utils
    {
        std::string to_string(const std::wstring &wstring);
        std::wstring to_wstring(const std::string &string);

        /**
         * @brief Convert a IfcBoundingBox to a carve AABB. To be removed as soon as this functionnality is implemented by IFC++
         * 
         * @param bbox The IfcBoundingBox to convert
         * @param representation_converter Smart pointer to a RepresentationConverter
         * @param precision Mesures precision (closest 10^precision)
         * @return carve::geom3d::AABB The converted bounding box as AABB
         */
        carve::geom3d::AABB compute_aabb(std::shared_ptr<IfcBoundingBox> bbox, std::shared_ptr<RepresentationConverter> representation_converter, const int32_t precision = 0);
        Polygon2 compute_profile(std::shared_ptr<IfcSweptAreaSolid> solid_model, std::shared_ptr<RepresentationConverter> representation_converter);
        carve::math::Matrix compute_placement(std::shared_ptr<IfcObjectPlacement> placement, std::shared_ptr<RepresentationConverter> representation_converter);
        carve::math::Matrix compute_placement(std::shared_ptr<IfcPlacement> placement, std::shared_ptr<RepresentationConverter> representation_converter);

        Polygon2 polylineset_to_polygon(const std::shared_ptr<carve::input::PolylineSetData> polyline_set);
        carve::geom2d::P2Vector aabb_to_polyline(const carve::geom3d::AABB& aabb);
        void close_polygon(Polygon2& poly);

        carve::geom2d::P2Vector transform(const carve::geom2d::P2Vector &loop, const carve::math::Matrix &transform);
        Polygon2 transform(const Polygon2& poly, const carve::math::Matrix &transform);
    } // namespace utils
} // namespace pathfinder

std::ostream &operator<<(std::ostream &os, const carve::geom2d::P2Vector &polyline);
std::ostream &operator<<(std::ostream &os, const pathfinder::Polygon2 &polygon);
