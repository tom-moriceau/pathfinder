#pragma once

#include "ifcpp/geometry/Carve/GeometryConverter.h"
#include "ifcpp/reader/ReaderSTEP.h"

#include "nlohmann/json.hpp"

#include "pathfinder/Storey.hpp"
#include "pathfinder/PathFinder.hpp"

using json = nlohmann::json;

namespace pathfinder
{
    class Building
    {
    private:
        GeometryConverter m_geometry_converter;
        std::vector<PathNetwork> m_path_networks;

        std::vector<StoreyPtr> m_storeys;

    public:
        Building(std::shared_ptr<BuildingModel> ifc_model);

        void compute_path_networks();

        json to_json() const;
    };
}