#pragma once

#include <string>
#include <locale>
#include <codecvt>

#include "carve/geom2d.hpp"
#include "carve/matrix.hpp"
#include "clipper2/clipper.h"
#include "clipper2/clipper.core.h"

#include "pathfinder/utils.hpp"

namespace pathfinder
{
    namespace utils
    {
        typedef enum
        {
            PolygonUnion,
            PolygonIntersection,
            PolygonDifference,
            PolygonXor
        } PolygonBooleanOperation;

        typedef enum
        {
            PolylineIntersection,
            PolylineDifference
        } PolylineBooleanOperation;

        //
        //  Misc functions
        //
        std::vector<carve::geom2d::P2Vector> slit_to_segments(const carve::geom2d::P2Vector &polyline);
        carve::geom2d::P2Vector join_from_segments(const std::vector<carve::geom2d::P2Vector> &segments);

        //
        //  Boolean function
        //

        std::vector<Polygon2> polygon_boolean(const Polygon2 &subject, const Polygon2 &clip, PolygonBooleanOperation op);
        std::vector<Polygon2> polygon_union(const std::vector<Polygon2> &subjects);

        std::vector<carve::geom2d::P2Vector> polyline_boolean(const carve::geom2d::P2Vector &subject, const Polygon2 &clip, PolylineBooleanOperation op);
        std::vector<carve::geom2d::P2Vector> polyline_boolean(const std::vector<carve::geom2d::P2Vector> &subject, const std::vector<Polygon2> & clip, PolylineBooleanOperation op);
    } // namespace utils
} // namespace pathfinder
