#pragma once

#include "ifcpp/IFC4/include/IfcDoor.h"
#include "ifcpp/IFC4/include/IfcRelDefinesByType.h"
#include "ifcpp/geometry/Carve/RepresentationConverter.h"

#include "carve/geom2d.hpp"

#include "nlohmann/json.hpp"

#include "pathfinder/utils.hpp"
#include "pathfinder/geojson.hpp"

using json = nlohmann::json;

#define RESCALE_FACTOR 1.2

namespace pathfinder
{
    class Door
    {
    private:
        std::vector<carve::geom2d::P2> m_bounding_box;

    public:
        Door(std::shared_ptr<IfcDoor> &door, std::shared_ptr<RepresentationConverter> converter);

        Polygon2 as_polygon() const;
        carve::geom2d::P2Vector as_polyline() const;

        json to_json() const;
    };

    typedef std::shared_ptr<Door> DoorPtr;
}