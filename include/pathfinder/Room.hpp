#pragma once

#include <stdexcept>
#include <cstdint>

#include "ifcpp/IFC4/include/IfcSpace.h"
#include "ifcpp/IFC4/include/IfcRelAggregates.h"
#include "ifcpp/IFC4/include/IfcRelSpaceBoundary.h"
#include "ifcpp/IFC4/include/IfcElement.h"
#include "ifcpp/IFC4/include/IfcProductRepresentation.h"
#include "ifcpp/IFC4/include/IfcRepresentation.h"
#include "ifcpp/IFC4/include/IfcLabel.h"
#include "ifcpp/IFC4/include/IfcSweptAreaSolid.h"
#include "ifcpp/geometry/Carve/RepresentationConverter.h"
#include "carve/geom2d.hpp"

#include "nlohmann/json.hpp"

#include "pathfinder/Door.hpp"
#include "pathfinder/utils.hpp"
#include "pathfinder/geojson.hpp"
#include "pathfinder/geometry.hpp"

using json = nlohmann::json;

namespace pathfinder
{
    class Room
    {
    private:
        size_t m_id;
        size_t m_storey_id;
        std::vector<Polygon2> m_polygons;
        /**
         * @brief Doors that intersect with the room and will be merged with the room polygon
         */
        std::set<DoorPtr> m_doors;
        /**
         * @brief Doors that intersect with the room, but that will not be merged with the room polygon
         */
        std::set<DoorPtr> m_connected_doors;

        static int16_t get_representation_priority(std::shared_ptr<IfcRepresentation> representation);

    public:
        Room(std::shared_ptr<IfcSpace> &space, std::shared_ptr<RepresentationConverter> converter, size_t storey_idx, size_t room_idx);
        Room(std::vector<Polygon2> polygons, size_t storey_idx, size_t room_idx);

        double door_covering_area(const DoorPtr door) const;

        void add_door(DoorPtr door);
        void remove_door(DoorPtr door);
        void add_connected_door(DoorPtr door);
        void remove_connected_door(DoorPtr door);

        std::string get_id_str() const;
        size_t get_storey_id() const;
        size_t get_room_id() const;
        std::vector<Polygon2> get_polygons() const;

        /**
         * @brief Get the room polygons after merging the rooms ones
         *
         * @return std::vector<Polygon2> Vector containing every polygons of the room, merged with the doors polygons
         */
        std::vector<Polygon2> get_complete_polygons() const;
        std::vector<carve::geom2d::P2Vector> get_complete_walls() const;

        json to_json() const;
    };

    typedef std::shared_ptr<Room> RoomPtr;
}