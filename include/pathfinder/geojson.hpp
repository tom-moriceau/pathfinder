#pragma once

#include <string>
#include <cstdint>

#include "carve/geom2d.hpp"
#include "carve/matrix.hpp"

#include "nlohmann/json.hpp"

#include "pathfinder/utils.hpp"

using json = nlohmann::json;

namespace pathfinder
{
    namespace geojson
    {
        typedef struct {
            std::string fill_color;
            float opacity;
        } GeoJsonStyle;

        json polygon_to_json(const Polygon2 &polygon);
        json polyline_to_json(const carve::geom2d::P2Vector &polyline);
        json style_to_json(const GeoJsonStyle style);
    } // namespace utils
} // namespace pathfinder
